package com.blackjack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A hand contains a player or dealer cards
 */
public class Hand {
    private final List<Card> others = new ArrayList<Card>();
    private final List<Card> aces = new ArrayList<Card>();
    private final boolean fromSplit;
    private boolean playDouble;

    public Hand(boolean fromSplit) {
        this.fromSplit = fromSplit;
    }

    public int value(){
        int value = 0;
        for (Card card : others){
            value += card.getValue().getValue1();
        }

        for(Card card : aces){
            if(value + card.getValue().getValue2() > 21){
                value += card.getValue().getValue1();
            } else {
                value += card.getValue().getValue2();
            }
        }

        return value;
    }

    public void addCard(Card card){
        if(card.getValue() == CardValue.Ace){
            aces.add(card);
        } else {
            others.add(card);
        }
    }

    public boolean isFromSplit() {
        return fromSplit;
    }

    public void clearHand(){
        aces.clear();
        others.clear();
    }

    public void setPlayDouble(boolean playDouble) {
        this.playDouble = playDouble;
    }

    public boolean isPlayDouble() {
        return playDouble;
    }

    public boolean bust() {
        return value() > 21;
    }

    public boolean blackjack() {
        return value() == 21  && aces.size() + others.size() == 2;
    }

    public int handSize() {
        return aces.size() + others.size();
    }

    public boolean canSplit(){
        // todo can you split on 3 cards?
        return handSize() == 2 &&
                (aces.size() == 2 || others.stream().collect(Collectors.groupingBy(x -> x.getValue())).size() == 1);
    }

    @Override
    public String toString() {
        String result = "";

        for (Card card : others){
            result += card + "\n";
        }

        for (Card card : aces) {
            result += card + "\n";
        }

        return result + "value: " + value();
    }

    public boolean hasAce() {
        return aces.size() > 0;
    }

    public List<Card> cards() {
        ArrayList<Card> cards = new ArrayList<>(aces);
        cards.addAll(others);
        return cards;
    }
}
