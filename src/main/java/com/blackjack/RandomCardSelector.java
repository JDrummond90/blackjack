package com.blackjack;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Random;

/**
 * Created by jamiedrummond on 02/04/2016.
 */
public class RandomCardSelector implements CardSelector {
    private Random random = new Random();

    public Card selectCard(@NotNull List<Card> cards) {
        if(cards.size() <= 0){
            throw new IllegalArgumentException("No cards to select");
        }

        int selected = random.nextInt(cards.size());
        Card card = cards.get(selected);
        cards.remove(selected);
        return card;
    }
}
