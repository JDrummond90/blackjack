package com.blackjack;

import java.util.Objects;

/**
 * Created by jamiedrummond on 02/04/2016.
 */
public enum CardValue {
    Two(2),
    Three(3),
    Four(4),
    Five(5),
    Six(6),
    Seven(7),
    Eight(8),
    Nine(9),
    Ten(10),
    Jack(10),
    Queen(10),
    King(10),
    Ace(1, 11);

    private final int value2;
    private final int value1;

    CardValue(int value) {
        this.value1 = value;
        this.value2 = 0;
    }

    CardValue(int value1, int value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public int getValue2() {
        return value2;
    }

    public int getValue1() {
        return value1;
    }
}