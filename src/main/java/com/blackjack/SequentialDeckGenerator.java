package com.blackjack;

import java.util.ArrayList;
import java.util.List;

/**
 * Generates a deck sequentially, that is a full 52 card deck followed by another until
 * the number required is provided
 */
public class SequentialDeckGenerator implements DeckGenerator {
    public List<Card> generateDeck(int decks) {
        List<Card> cards = new ArrayList<>(decks * (Suit.values().length * CardValue.values().length));

        for (int i = 0; i < decks; i++) {
            for (Suit suit : Suit.values()) {
                for (CardValue value : CardValue.values()) {
                    cards.add(new Card(suit, value));
                }
            }
        }

        return cards;
    }
}
