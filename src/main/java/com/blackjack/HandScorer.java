package com.blackjack;

/**
 * Created by jamie on 07/01/18.
 */
public class HandScorer {

    public Result handResult(Hand playerHand, Hand dealerHand) {
        Result result = Result.Loss;

        if (playerHand.blackjack()) {
            result = dealerHand.blackjack() ? Result.Push : Result.Blackjack;
        } else if (!playerHand.bust() && !dealerHand.blackjack()) {
            if (dealerHand.bust() || dealerHand.value() < playerHand.value()) {
                result = Result.Win;
            } else if (dealerHand.value() == playerHand.value()) {
                result = Result.Push;
            }
        }

        return result;
    }
}
