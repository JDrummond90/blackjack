package com.blackjack;

import java.util.List;

/**
 * Created by jamiedrummond on 02/04/2016.
 */
public interface CardSelector {

    Card selectCard(List<Card> cards);
}
