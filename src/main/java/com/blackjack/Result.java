package com.blackjack;

/**
 * Created by jamiedrummond on 03/04/2016.
 */
public enum Result {
    Blackjack(1),
    Win(1),
    Loss(-1),
    Push(0);

    Result(int multiplier) {
        this.multiplier = multiplier;
    }

    int multiplier;

    int applyResultToBet(int bet) {
        int result = bet * multiplier;
        if (this == Blackjack) {
            result += bet / 2;
        }
        return result;
    }
}
