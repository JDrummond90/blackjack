package com.blackjack;

/**
 * Created by jamiedrummond on 02/04/2016.
 */
public enum Action {
    Stand,
    Hit,
    Double,
    Split
}
