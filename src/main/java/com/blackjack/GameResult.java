package com.blackjack;

import java.util.List;

/**
 * Created by jamie on 07/01/18.
 */
public class GameResult {

    private final int winnings;
    private final List<Result> handResults;

    public int getWinnings() {
        return winnings;
    }

    public List<Result> getHandResults() {
        return handResults;
    }

    public GameResult(int winnings, List<Result> handResults) {

        this.winnings = winnings;
        this.handResults = handResults;
    }
}
