package com.blackjack;

/**
 * Created by jamiedrummond on 02/04/2016.
 */
public interface BlackJackStrategy {

    Action playHand(Hand myCards, Hand dealerHand);

    int placeBet();
}
