package com.blackjack;

/**
 * Created by jamiedrummond on 02/04/2016.
 */
public class Card {

    private CardValue value;
    private Suit suit;

    public Card(Suit suit, CardValue value) {
        this.suit = suit;
        this.value = value;
    }

    public CardValue getValue() {
        return value;
    }

    public Suit getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return suit.getValue() + "" + value;
    }
}
