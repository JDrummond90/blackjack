package com.blackjack.strategies;


import com.blackjack.Action;
import com.blackjack.BlackJackStrategy;
import com.blackjack.CardValue;
import com.blackjack.Hand;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OptimalStrategyUnsized implements BlackJackStrategy {

    private List<Rule> hardRules = new ArrayList<>();
    private List<Rule> softRules = new ArrayList<>();
    private List<Rule> splitRules = new ArrayList<>();

    public OptimalStrategyUnsized() {
        loadRules();
    }

    class Rule {
        boolean includesLower;
        boolean includesHigher;
        Action action;
        CardValue dealerValue;
        /**
         * the value of the hand, in the case of the split this is the single repeated value e.g. 6 in a 6,6 split
         */
        int handValue;
        boolean isSplit;

        public Rule(boolean includesLower, boolean includesHigher, Action action, CardValue dealerValue, int handValue, boolean isSplit) {
            this.includesLower = includesLower;
            this.includesHigher = includesHigher;
            this.action = action;
            this.dealerValue = dealerValue;
            this.handValue = handValue;
            this.isSplit = isSplit;
        }

        public Action whatIf(Hand playerCards, Hand dealerHand) {

            if (dealerHand.handSize() > 1) {
                throw new IllegalArgumentException();
            } else if (dealerHand.cards().get(0).getValue() == dealerValue) {

                if (playerCards.canSplit() && isSplit && playerCards.cards().get(0).getValue().getValue1() == handValue) {
                    return action;
                }

                boolean lowerHit = includesLower && playerCards.value() <= handValue;
                boolean upperHit = includesHigher && playerCards.value() >= handValue;
                if (playerCards.value() == handValue || lowerHit || upperHit) {
                    return action;
                }
            }

            return null;
        }
    }

    public static List<CardValue> matchingCards(String cardString) {
        List<CardValue> cardValues = new ArrayList<>();
        if (Objects.equals(cardString, "A")) {
            cardValues.add(CardValue.Ace);
        } else {
            for (CardValue cardValue : CardValue.values()) {
                if (Objects.equals(String.valueOf(cardValue.getValue1()), cardString)) {
                    cardValues.add(cardValue);
                }
            }
        }
        return cardValues;
    }

    private void loadRules() {
        try {
            List<String> configLines = Files.readAllLines(Paths.get("src/main/resources/rules.txt"));

            List<Rule> load = null;
            String[] dealerCardValues = new String[0];

            for (String configLine : configLines) {
                if (configLine.startsWith("rule")) {
                    load = setToBeLoaded(configLine);
                } else if (configLine.startsWith("#") || configLine.trim().length() == 0) {
                    // Comment
                } else if (configLine.startsWith("\t")) {
                    // dealer numbers
                    dealerCardValues = configLine.trim().split("\t");
                } else {
                    createRule(load, dealerCardValues, configLine);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createRule(List<Rule> load, String[] dealerCardValues, String configLine) {
        String[] elements = configLine.split("\t");
        assert elements.length == dealerCardValues.length + 1 : configLine;

        String playerCardValue = elements[0].split(",")[0];

        boolean includesLower = playerCardValue.contains("-");
        boolean includesHigher = playerCardValue.contains("+");

        if (Objects.equals(playerCardValue, "A")) {
            playerCardValue = "1";
        }
        int handValue = Integer.parseInt(playerCardValue.replace("+", "").replace("-", ""));

        for (int i = 0; i < dealerCardValues.length; i++) {
            String dealersNumber = dealerCardValues[i];

            List<CardValue> dealerCard = matchingCards(dealersNumber);
            for (CardValue cardValue : dealerCard) {
                Rule rule = new Rule(includesLower, includesHigher, createAction(elements[i + 1].trim()),
                        cardValue, handValue, load == splitRules);
                load.add(rule);
            }
        }
    }

    private Action createAction(String element) {
        switch (element.toUpperCase()) {
            case "H":
                return Action.Hit;
            case "S":
                return Action.Stand;
            case "DH":
                return Action.Double;
            case "DS":
                return Action.Double;
            case "P":
                return Action.Split;
            case "PH":
                return Action.Split;
            case "RH":
                return Action.Hit;//Action.Surrender;
        }
        throw new IllegalArgumentException("No action for element " + element);
    }

    private List<Rule> setToBeLoaded(String configLine) {
        switch (configLine.replace("rule", "").trim()) {
            case "hard":
                return hardRules;
            case "soft":
                return softRules;
            case "split":
                return splitRules;
        }
        return null;
    }

    @Override
    public Action playHand(Hand playerCards, Hand dealerHand) {

        // todo shouldnt get this
        if (playerCards.value() == 21) {
            return Action.Stand;
        }

        // max 1 split (possibly a game rule?)
        if (playerCards.canSplit() && !playerCards.isFromSplit()) {
            for (Rule splitRule : splitRules) {
                Action action = splitRule.whatIf(playerCards, dealerHand);
                if (action != null) {
                    return action;
                }
            }
        } else {
            // hard vs soft
            if (playerCards.hasAce()) {
                for (Rule softRule : softRules) {
                    Action action = softRule.whatIf(playerCards, dealerHand);
                    if (action != null) {
                        return action;
                    }
                }
            } else {
                for (Rule hardRule : hardRules) {
                    Action action = hardRule.whatIf(playerCards, dealerHand);
                    if (action != null) {
                        return action;
                    }
                }
            }
        }

        throw new IllegalStateException("Unable to find a playable rule\nPlayerHand:\n" + playerCards + "\nDealer:" + dealerHand);
    }

    @Override
    public int placeBet() {
        return 50;
    }
}