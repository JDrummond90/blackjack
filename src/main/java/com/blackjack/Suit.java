package com.blackjack;

/**
 * Created by jamiedrummond on 02/04/2016.
 */
public enum Suit {
    Hearts("♥"),
    Spades("♠"),
    Clubs("♣"),
    Diamonds("♦");

    private String value;

    Suit(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Suit{" +
                "value='" + value + '\'' +
                '}';
    }
}
