package com.blackjack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jamiedrummond on 02/04/2016.
 */
public class Game {
    private static Logger logger = LogManager.getLogger();

    private int decks = 6;
    private int dealerHitUntil = 16;
    private int numberOfHands = 20;

    private final DeckGenerator deckGenerator;
    private final CardSelector cardSelector;
    private final BlackJackStrategy strategy;
    private final HandScorer handScorer = new HandScorer();

    public Game(CardSelector cardSelector, DeckGenerator deckGenerator,
                BlackJackStrategy strategy,
                int dealerHitUntil, int decks) {
        this.cardSelector = cardSelector;
        this.deckGenerator = deckGenerator;
        this.strategy = strategy;
        this.dealerHitUntil = dealerHitUntil;
        this.decks = decks;
    }

    public GameResult runGame() {
        int hands = 0;
        List<Result> results = new ArrayList<>();
        List<Card> cards = deckGenerator.generateDeck(decks);

        int winnings = 0;

        while (++hands < numberOfHands) {
            int playerBet = strategy.placeBet();
            Hand playerHand = new Hand(false);

            playerHand.addCard(cardSelector.selectCard(cards));

            Hand dealerHand = new Hand(false);
            // Dealer second card is face down until after player stands/busts action made
            dealerHand.addCard(cardSelector.selectCard(cards));

            playerHand.addCard(cardSelector.selectCard(cards));
            final Card dealerCard2 = cardSelector.selectCard(cards);

            List<Hand> playerHands = Arrays.asList(playerHand);

            if (!playerHand.blackjack()) {
                playerHands = playHand(cards, playerHand, dealerHand);
            }

            dealerHand.addCard(dealerCard2);
            playDealerHand(cards, dealerHand);

            for (Hand hand : playerHands) {
                int handBet = playerBet;
                if(hand.isPlayDouble()) {
                    handBet = handBet << 1;
                }

                Result result = handScorer.handResult(hand, dealerHand);
                int handValue = result.applyResultToBet(handBet);
                winnings += handValue;

                logger.debug("Hand complete, from split {}, result {}, bet {}, handResult {}, cards left {}, winnings {}",
                        hand.isFromSplit(), result, handBet, handValue, cards.size(), winnings);
                logger.debug("Player hand:\n{}\nDealer hand:\n{}", hand, dealerHand);

                results.add(result);
            }
        }

        logger.info("Game complete, hands {}\nblackjack:{} wins:{} pushes:{} losses: {} winnings: {}", hands,
                results.stream().filter(x -> x == Result.Blackjack).count(),
                results.stream().filter(x -> x == Result.Win).count(),
                results.stream().filter(x -> x == Result.Push).count(),
                results.stream().filter(x -> x == Result.Loss).count(),
                winnings);

        return new GameResult(winnings, results);
    }

    private List<Hand> playHand(List<Card> cards, Hand playerHand, Hand dealerHand) {
        List<Hand> playerHands = new ArrayList<>();
        playerHands.add(playerHand);

        Action action;
        do {
            action = strategy.playHand(playerHand, dealerHand);

            if (action == Action.Split) {
                if (!playerHand.canSplit()) {
                    throw new IllegalArgumentException("Unable to split with hand " + playerHand);
                }
                // TODO can you split with 2/3 cards
                playerHands.remove(playerHand);
                Hand firstHand = new Hand(true);
                firstHand.addCard(playerHand.cards().get(0));
                firstHand.addCard(cardSelector.selectCard(cards));
                playerHands.add(firstHand);
                playHand(cards, firstHand, dealerHand);

                Hand secondHand = new Hand(true);
                secondHand.addCard(playerHand.cards().get(1));
                secondHand.addCard(cardSelector.selectCard(cards));
                playerHands.add(secondHand);
                playHand(cards, secondHand, dealerHand);
                break;
            } else if (action == Action.Hit || action == Action.Double) {
                if(action == Action.Double){
                    playerHand.setPlayDouble(true);
                }
                Card card = cardSelector.selectCard(cards);
                playerHand.addCard(card);
            }
        } while (action == Action.Hit && !playerHand.bust());


        return playerHands;
    }

    private void playDealerHand(List<Card> cards, Hand dealerHand) {
        while (!(dealerHand.value() >= dealerHitUntil || dealerHand.bust())) {
            Card card = cardSelector.selectCard(cards);
            dealerHand.addCard(card);
        }
    }

}
