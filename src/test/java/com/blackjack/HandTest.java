package com.blackjack;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by jamiedrummond on 03/04/2016.
 */
public class HandTest {

    @Test
    public void Hand_Should_Show_Blackjack_Only_On_Deal(){
        {
            Hand blackjack = new Hand(false);
            blackjack.addCard(new Card(Suit.Hearts, CardValue.Ace));
            blackjack.addCard(new Card(Suit.Hearts, CardValue.Jack));

            assertEquals(true, blackjack.blackjack());
        }

        {
            Hand threeCard21 = new Hand(false);
            threeCard21.addCard(new Card(Suit.Hearts, CardValue.Ace));
            threeCard21.addCard(new Card(Suit.Hearts, CardValue.Five));
            threeCard21.addCard(new Card(Suit.Hearts, CardValue.Five));

            assertEquals(false, threeCard21.blackjack());
            assertEquals(21, threeCard21.value());
        }
    }

    @Test
    public void Hand_Should_Indicate_Bust_Hand(){
        Hand hand = new Hand(false);
        hand.addCard(new Card(Suit.Hearts, CardValue.Five));
        hand.addCard(new Card(Suit.Hearts, CardValue.Jack));
        hand.addCard(new Card(Suit.Hearts, CardValue.Jack));

        assertEquals(true, hand.bust());
    }

    @Test
    public void Hand_Value_With_Low_Ace_If_High_Is_Bust(){
        Hand hand = new Hand(false);
        hand.addCard(new Card(Suit.Hearts, CardValue.Five));
        hand.addCard(new Card(Suit.Hearts, CardValue.Jack));
        hand.addCard(new Card(Suit.Hearts, CardValue.Ace));

        assertEquals(16, hand.value());
        assertEquals(false, hand.bust());
    }

    @Test
    public void Hand_Should_Reset(){
        Hand hand = new Hand(false);
        hand.addCard(new Card(Suit.Hearts, CardValue.Five));
        hand.clearHand();

        assertEquals(0, hand.value());
        assertEquals(false, hand.blackjack());
        assertEquals(false, hand.bust());
    }

}