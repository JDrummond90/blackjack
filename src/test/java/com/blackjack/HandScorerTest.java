package com.blackjack;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by jamie on 07/01/18.
 */
public class HandScorerTest {
    @Test
    public void bustDealerIsWin() throws Exception {

        Hand dealerHand = new Hand(false);
        dealerHand.addCard(new Card(Suit.Hearts, CardValue.King));
        dealerHand.addCard(new Card(Suit.Hearts, CardValue.Six));
        dealerHand.addCard(new Card(Suit.Hearts, CardValue.Seven));

        Hand playerHand = new Hand(false);
        playerHand.addCard(new Card(Suit.Hearts, CardValue.King));
        playerHand.addCard(new Card(Suit.Hearts, CardValue.Five));

        HandScorer scorer = new HandScorer();
        Result result = scorer.handResult(playerHand, dealerHand);

        assertEquals(Result.Win, result);
    }

    @Test
    public void blackjackBeatsHard21() throws Exception {

        Hand dealerHand = new Hand(false);
        dealerHand.addCard(new Card(Suit.Hearts, CardValue.King));
        dealerHand.addCard(new Card(Suit.Hearts, CardValue.Ace));

        Hand playerHand = new Hand(false);
        playerHand.addCard(new Card(Suit.Hearts, CardValue.King));
        playerHand.addCard(new Card(Suit.Hearts, CardValue.Five));
        playerHand.addCard(new Card(Suit.Hearts, CardValue.Six));

        HandScorer scorer = new HandScorer();
        Result result = scorer.handResult(playerHand, dealerHand);

        assertEquals(Result.Loss, result);
    }

    @Test
    public void bothBlackjackPush() throws Exception {

        Hand dealerHand = new Hand(false);
        dealerHand.addCard(new Card(Suit.Hearts, CardValue.King));
        dealerHand.addCard(new Card(Suit.Hearts, CardValue.Ace));

        Hand playerHand = new Hand(false);
        playerHand.addCard(new Card(Suit.Hearts, CardValue.King));
        playerHand.addCard(new Card(Suit.Hearts, CardValue.Ace));

        HandScorer scorer = new HandScorer();
        Result result = scorer.handResult(playerHand, dealerHand);

        assertEquals(Result.Push, result);
    }

}