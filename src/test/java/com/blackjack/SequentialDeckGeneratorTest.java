package com.blackjack;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Created by jamiedrummond on 03/04/2016.
 */
public class SequentialDeckGeneratorTest {

    @Test
    public void Deck_Should_Provide_Correct_Number_Of_Decks(){
        SequentialDeckGenerator generator = new SequentialDeckGenerator();

        List<Card> cards = generator.generateDeck(4);

        assertEquals(52*4, cards.size());
    }

    @Test
    public void Deck_Should_Contain_Correct_Cards(){
        SequentialDeckGenerator generator = new SequentialDeckGenerator();
        List<Card> cards = generator.generateDeck(1);

        assertEquals(52, cards.size());

        Map<CardValue, List<Card>> groups = cards.stream().collect(Collectors.groupingBy(Card::getValue));

        for (Map.Entry<CardValue, List<Card>> group : groups.entrySet()){
            List<Suit> suits = new ArrayList<>();
            for (Card card : group.getValue()) {
                assertFalse(suits.contains(card.getSuit()));
                suits.add(card.getSuit());
            }
        }

    }

}