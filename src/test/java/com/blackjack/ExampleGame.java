package com.blackjack;

import org.junit.Test;

/**
 * Created by jamiedrummond on 02/04/2016.
 */
public class ExampleGame {

    @Test
    public void Game_Test(){

        BlackJackStrategy strategy = new BlackJackStrategy() {
            @Override
            public Action playHand(Hand myCards, Hand dealerHand){
                if(dealerHand.value() >= 10 && myCards.value() <= 15){
                    return Action.Hit;
                } else if (myCards.value() <= 11) {
                    return Action.Hit;
                } else {
                    return Action.Stand;
                }
            }

            @Override
            public int placeBet() {
                return 50;
            }
        };


        CardSelector selector = new RandomCardSelector();
        DeckGenerator generator = new SequentialDeckGenerator();
        Game game = new Game(selector, generator, strategy, 16, 6);

        game.runGame();




    }

}