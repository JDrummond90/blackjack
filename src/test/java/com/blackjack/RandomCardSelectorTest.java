package com.blackjack;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RandomCardSelectorTest {

    private List<Card> cards;

    @Test
    public void Selector_Should_Return_Card_In_Range(){
        RandomCardSelector selector = new RandomCardSelector();
        Card card = selector.selectCard(cards);

        assertNotNull(card);
    }

    @Test
    public void Selector_Should_Remove_Card(){
        RandomCardSelector selector = new RandomCardSelector();
        Card card = selector.selectCard(cards);

        assertFalse(cards.contains(card));
    }

    @Test
    public void Selector_Should_Exhaust_List(){
        RandomCardSelector selector = new RandomCardSelector();
        Card card1 = selector.selectCard(cards);
        Card card2 = selector.selectCard(cards);
        Card card3 = selector.selectCard(cards);

        assertNotNull(card3);
        assertTrue(card1 != card2 && card2 != card3 && card1 != card3);
        assertEquals(0, cards.size());
    }

    @Test(expected = Exception.class)
    public void Selector_Should_Reject_Empty_List(){
        RandomCardSelector selector = new RandomCardSelector();
        selector.selectCard(new ArrayList<>());
    }

    @Before
    public void setup() {
        cards = new ArrayList<>();
        cards.add(new Card(Suit.Clubs, CardValue.Ace));
        cards.add(new Card(Suit.Spades, CardValue.Eight));
        cards.add(new Card(Suit.Hearts, CardValue.Jack));
    }

}