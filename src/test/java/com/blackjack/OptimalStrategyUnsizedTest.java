package com.blackjack;

import com.blackjack.strategies.OptimalStrategyUnsized;
import org.apache.logging.log4j.LogManager;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

/**
 * Created by jamie on 31/03/17.
 */
public class OptimalStrategyUnsizedTest {
    @Test
    public void playHand() throws Exception {
        // todo random for now

        OptimalStrategyUnsized optimalStrategy = new OptimalStrategyUnsized();
        CardSelector selector = new RandomCardSelector();
        DeckGenerator generator = new SequentialDeckGenerator();
        Game game = new Game(selector, generator, optimalStrategy, 16, 6);


        List<Result> totalResults = new ArrayList<>();
        int totalWinnings = 0;
        for (int i = 0; i < 10000; i++) {
            GameResult gameResult = game.runGame();
            totalResults.addAll(gameResult.getHandResults());
            totalWinnings += gameResult.getWinnings();
        }

        LogManager.getLogger().info("Game complete, hands {}\nblackjack:{} wins:{} pushes:{} losses: {}, winnings: {}",
                1,
                totalResults.stream().filter(x -> x == Result.Blackjack).count(),
                totalResults.stream().filter(x -> x == Result.Win).count(),
                totalResults.stream().filter(x -> x == Result.Push).count(),
                totalResults.stream().filter(x -> x == Result.Loss).count(),
                totalWinnings);
    }

    @Test
    public void royalDealerCard(){
        OptimalStrategyUnsized optimalStrategy = new OptimalStrategyUnsized();

        Hand myHand = new Hand(false);
        myHand.addCard(new Card(Suit.Clubs, CardValue.Three));
        myHand.addCard(new Card(Suit.Diamonds, CardValue.Five));

        Hand dealerHand = new Hand(false);
        dealerHand.addCard(new Card(Suit.Hearts, CardValue.Queen));

        Action action = optimalStrategy.playHand(myHand, dealerHand);
        assertEquals(Action.Hit, action);
    }

    @Test
    public void upperBoundCheck(){
        OptimalStrategyUnsized optimalStrategy = new OptimalStrategyUnsized();

        Hand myHand = new Hand(false);
        myHand.addCard(new Card(Suit.Clubs, CardValue.Eight));
        myHand.addCard(new Card(Suit.Diamonds, CardValue.King));

        Hand dealerHand = new Hand(false);
        dealerHand.addCard(new Card(Suit.Hearts, CardValue.Four));

        Action action = optimalStrategy.playHand(myHand, dealerHand);
        assertEquals(Action.Stand, action);

    }

}